const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const path = require('path');

const multer = require('multer');
// we configure the multer storage
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads'); // Define the destination - File where the file will be stored
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname); // Define the filename - Change here if you want
    }
});

let messages = [];
let conversations = [];
let users = [];
const upload = multer({storage: storage});
// Load messages and users from the JSON files
try {
    messages = JSON.parse(fs.readFileSync('messages.json', 'utf-8'));
    conversations = JSON.parse(fs.readFileSync('conversations.json', 'utf-8'));
    users = JSON.parse(fs.readFileSync('users.json', 'utf-8'));
} catch (err) {
    console.log('No existing data found. Starting with empty lists.');
}
const cors = require('cors');
let app = express();
app.use(cors());
app.use(express.json());
const server = http.createServer(app);

app.post('/login', (req, res) => {
    const { email, password } = req.body;

    // Check if the user exists
    const user = users.find(user => user.email === email);
    if (!user) {
        return res.status(400).send('User does not exist');
    }

    // Check if the password is correct
    if(password === user.password ) {
        // if the user is registered we return a jwt token
        const token = jwt.sign({ userId: user.id }, 'cdKolz155zd6qzAFzzx');
        res.status(200).json({ token , id :user.id , email:user.email , role : user.role});
    }
    else{
        return res.status(400).send('Invalid Password');
    }
});

app.get('/getUserByToken', (req, res) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    if (!token) {
        return res.status(403).send('A token is required for authentication');
    }

    try {
        const decoded = jwt.verify(token, 'cdKolz155zd6qzAFzzx');
        const user = users.find(user => user.id === decoded.userId);

        if (!user) {
            return res.status(404).send('User not found');
        }

        res.status(200).json(user);
    } catch (err) {
        return res.status(401).send('Invalid Token');
    }
});

app.post('/upload', upload.single('file'), (req, res, next) => {
    res.send(req.file.filename);
});

app.get('/getFile/:filename', (req, res) => {
    let filename = req.params.filename;
    let filePath = path.join(__dirname, '/uploads/') + filename;

    // Check if file exists
    fs.access(filePath, fs.constants.F_OK, (err) => {
        if (err) {
            console.log("File does not exist");
            res.status(404).json({message: "File not found"});
        } else {
            // Here, we simply pipe the read stream (the file content) to the response.
            // This allows the client to download it.
            res.setHeader("Content-Disposition", 'attachment; filename="' + filename + '"');
            fs.createReadStream(filePath).pipe(res);
        }
    });
});

app.get('/getConversations', (req, res) => {
    res.status(200).json(conversations);
});

const io = socketIo(server, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"],
        allowedHeaders: ["my-custom-header"],
        credentials: true
    }
});

io.on('connection', (socket) => {


    // Send all existing messages to the connected client
    messages.forEach(message => socket.emit('message', message));

    socket.on('message', (message) => {
        // Save message to the JSON file
        // we update the message status
        message.status = 'sent';
        messages.push(message);
        fs.writeFileSync('messages.json', JSON.stringify(messages), 'utf-8');

        io.emit('message', message);
    });

});

server.listen(3000, () => {
    console.log('Server is running on port 3000');
});
