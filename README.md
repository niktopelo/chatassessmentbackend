# chat 

chat backend build in node js

## Launch with docker 
```bash
docker compose up 
```

## Install the dependencies without docker
```bash
yarn
# or
npm install
```

### Start the app 
```bash
yarn start
# or
npm start 
