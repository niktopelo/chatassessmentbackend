# Set the base image. (Node.js version 16 in this case)
FROM node:18-alpine3.17

# Set a working directory
WORKDIR /usr/src/app

# Copy package.json and yarn.lock
COPY package.json yarn.lock ./

# Install dependencies
RUN yarn install

# Bundle app source. Copy all your source code from current directory to the image
COPY . .

# Your app is listening on port 3000, so expose it
EXPOSE 3000

# Specifies what command to use to start the application
CMD [ "node", "your-app-start-file.js" ]
